package com.marcelorsjr.simplex;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONObject;

import com.marcelorsjr.simplex.ObjectiveFunction.Type;
import com.marcelorsjr.simplex.SimplexWetzel.SolutionResponse;
import com.marcelorsjr.simplex.integer.Node;
import com.marcelorsjr.simplex.integer.Node.PrumedReason;
import com.marcelorsjr.simplex.integer.Tree;

public class SimplexWetzelInteger {
	
	public double solution;
	public double variablesValues[];
	public Tree tree;
	public SolutionResponse solutionResponse;
	
	public SimplexWetzelInteger(SimplexWetzel simplexWetzel) throws Exception {
		double results[] = simplexWetzel.solve();
		
		if (simplexWetzel.solutionResponse != SolutionResponse.OPTIMAL_SOLUTION) {
			solutionResponse = simplexWetzel.solutionResponse;
		} else {
			solution = results[0];
			variablesValues = Arrays.copyOfRange(results, 1, results.length);
			

			tree = new Tree();
			solveForInteger2(simplexWetzel);
		}

		
	}
	
	public JSONObject solveToJson() {
		JSONObject jsonObject = new JSONObject();
		if (solutionResponse == SolutionResponse.PERMISSIVE_SOLUTION_DOES_NOT_EXIST) {
			JSONObject objectSolutionType = new JSONObject();
			objectSolutionType.put("id", 2);
			objectSolutionType.put("description", "Permissive solution does not exist");
			jsonObject.put("solution_type", objectSolutionType);
		} else if (solutionResponse == SolutionResponse.UNLIMITED_SOLUTION) {
			JSONObject objectSolutionType = new JSONObject();
			objectSolutionType.put("id", 3);
			objectSolutionType.put("description", "Unlimited solution");
			jsonObject.put("solution_type", objectSolutionType);
		} else if (solutionResponse == SolutionResponse.MULTIPLE_SOLUTIONS) {
			JSONObject objectSolutionType = new JSONObject();
			objectSolutionType.put("id", 4);
			objectSolutionType.put("description", "Multiple Solutions");
			jsonObject.put("solution_type", objectSolutionType);
		} else {
			jsonObject = tree.bestNode.nodeToJson();
			JSONObject objectSolutionType = new JSONObject();
			objectSolutionType.put("id", 5);
			objectSolutionType.put("description", "Integer Solution");
			jsonObject.put("solution_type", objectSolutionType);
		}
		
		return jsonObject;
	}
	
	private void solveForInteger(Node node, SimplexWetzel simplexWetzel) throws Exception {
//		simplexWetzel.fillFieldsWithCoefficients();
//		SolutionResponse solutionResponse = simplexWetzel.firstPhase();

		double results[] = simplexWetzel.solve();
		
		if (simplexWetzel.solutionResponse != SolutionResponse.OPTIMAL_SOLUTION) {
			node.prumed = true;
			node.prumedReason = PrumedReason.INFEASIBLE;
			//simplexWetzel.printSolution();
			return;
		}
		
	
		

		//simplexWetzel.printSolution();
		solution = results[0];


		node.solution = solution;
		double result;
		if (simplexWetzel.of.getType() == Type.MAXIMIZATION) {
			result = Double.MIN_VALUE;
		} else {
			result = Double.MAX_VALUE;
		}
		int iterator = 0;
		
		for (iterator = 1; iterator < results.length; iterator++) {
			double r = results[iterator];
			if (!((r == Math.floor(r)) && !Double.isInfinite(r))) {
				result = r;
			    break;
			}
		}
		
		//System.out.println(result);
		
		if (result != Double.MIN_VALUE && result != Double.MAX_VALUE) {
			String leftRestriction = "x"+iterator+" <= "+ Math.floor(result);
			String rightRestriction = "x"+iterator+" >= " + Math.ceil(result);	

			ArrayList<Restriction> list = new ArrayList(Arrays.asList(simplexWetzel.restrictions));

			list.add(new Restriction(leftRestriction, simplexWetzel.of.getCoefficients().length));
			Restriction[] newRestrictions = list.toArray(new Restriction[0]);
			
			node.left = new Node(newRestrictions.clone());
			list = new ArrayList(Arrays.asList(simplexWetzel.restrictions));
			list.add(new Restriction(rightRestriction, simplexWetzel.of.getCoefficients().length));
			newRestrictions = list.toArray(new Restriction[0]);
			node.right = new Node(newRestrictions.clone());

		} else {
			node.prumed = true;
			node.prumedReason = PrumedReason.OPTIMALITY;
			node.results = results;
		
			
			if (simplexWetzel.of.getType() == Type.MAXIMIZATION) {
				if (tree.bestNode.solution < node.solution)
					tree.bestNode = node;
			} else {
				if (tree.bestNode.solution > node.solution)
					tree.bestNode = node;
			}
			
			node.left = null;
			node.right = null;
		

			
		}
		if (node.left != null) {
			
			//System.out.println(tree.bestNode.solution);			
			solveForInteger(node.left, new SimplexWetzel(node.left.restrictions.clone(), simplexWetzel.of));
		}
		if (node.right != null) {

			//System.out.println(tree.bestNode.solution);
			solveForInteger(node.right, new SimplexWetzel(node.right.restrictions.clone(), simplexWetzel.of));
		
		}		
		
		
		

		
		
		
	}
	
	public void solveForInteger2(SimplexWetzel simplexWetzel) throws Exception {

		int iterator = 0;
		for (iterator = 0; iterator < variablesValues.length; iterator++) {
			double r = variablesValues[iterator];
			if (!((r == Math.floor(r)) && !Double.isInfinite(r))) {
			    break;
			}
		}
		
		if (iterator != variablesValues.length) {
			iterator++;
			String leftRestriction = "x"+iterator+" <= "+ Math.floor(variablesValues[iterator-1]);
			String rightRestriction = "x"+iterator+" >= " + Math.ceil(variablesValues[iterator-1]);	

			ArrayList<Restriction> list = new ArrayList(Arrays.asList(simplexWetzel.restrictions));

			list.add(new Restriction(leftRestriction, simplexWetzel.of.getCoefficients().length));
			Restriction[] newRestrictions = list.toArray(new Restriction[0]);
			
			tree.root.left = new Node(newRestrictions.clone());
			list = new ArrayList(Arrays.asList(simplexWetzel.restrictions));
			list.add(new Restriction(rightRestriction, simplexWetzel.of.getCoefficients().length));
			newRestrictions = list.toArray(new Restriction[0]);
			tree.root.right = new Node(newRestrictions.clone());

			
			
			tree.root.restrictions = simplexWetzel.restrictions;
			solveForInteger(tree.root.left, new SimplexWetzel(tree.root.left.restrictions.clone(), simplexWetzel.of));
			solveForInteger(tree.root.right, new SimplexWetzel(tree.root.right.restrictions.clone(), simplexWetzel.of));
			

			
		} else {
			tree.bestNode.solution = solution;
			
			double resultsAux[] = new double[variablesValues.length+1];
			resultsAux[0] = solution;
			for (int i = 1; i < resultsAux.length; i++) {
				resultsAux[i] = variablesValues[i-1];
			}
			
			tree.bestNode.results = resultsAux.clone();

			System.out.println("SOLUTION"+solution);
		}
		
		
	}
	
	
	
	
	

}
