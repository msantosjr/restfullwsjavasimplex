package com.marcelorsjr.simplex.integer;

import org.json.JSONObject;

import com.marcelorsjr.simplex.Restriction;
import com.marcelorsjr.simplex.ObjectiveFunction.Type;

public class Node {
	
	public enum PrumedReason {
		INFEASIBLE, OPTIMALITY, QUALITY
	}
	
	public Restriction[] restrictions;

	public Node right;
	public Node left;
	public double solution;
	public double results[];
	public boolean prumed;
	public PrumedReason prumedReason;
	
	public Node() {
		this.right = null;
		this.left = null;
		this.restrictions = null;
		prumedReason = null;
		solution = Double.MIN_VALUE;
	}
	
	public Node(Restriction[] restrictions) {
		this.right = null;
		this.left = null;
		this.restrictions = restrictions.clone();
		prumedReason = null;
		solution = Double.MIN_VALUE;
	}
	
	public JSONObject nodeToJson() {
		JSONObject jsonObject = new JSONObject();
	
		jsonObject.put("function_result", Math.abs(solution));
		

		for (int i = 1; i < results.length; i++)
			jsonObject.put("x"+i, results[i]);
			//System.out.println("x"+(i+1)+" = "+results[i]);
		
		return jsonObject;
		
	}
	
}
