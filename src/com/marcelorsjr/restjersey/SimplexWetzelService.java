package com.marcelorsjr.restjersey;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.jasper.xmlparser.SymbolTable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.marcelorsjr.simplex.ObjectiveFunction;
import com.marcelorsjr.simplex.Restriction;
import com.marcelorsjr.simplex.SimplexWetzel;
import com.marcelorsjr.simplex.SimplexWetzelInteger;
 
@Path("/simplexservice")
public class SimplexWetzelService {

	 @POST
	  @Produces("application/json")
	  @Consumes("application/json")
	  public Response simplexFromInput(InputStream incomingData) throws JSONException {
		  StringBuilder crunchifyBuilder = new StringBuilder();
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
				String line = null;
				while ((line = in.readLine()) != null) {
					crunchifyBuilder.append(line);
				}
			} catch (Exception e) {
				System.out.println("Error Parsing: - ");
			}
			System.out.println("Data Received: " + crunchifyBuilder.toString());
			
		JSONObject jo = new JSONObject(crunchifyBuilder.toString());
			
		boolean flagInt = jo.getBoolean("integer_problem");
		
		try {
			ObjectiveFunction of = new ObjectiveFunction(jo.getString("objective_function"));
			
			JSONArray arrRest = jo.getJSONArray("restrictions");
			
			Restriction[] r = new Restriction[arrRest.length()];

			
			for (int i = 0; i < r.length; i++) {
				r[i] = new Restriction(arrRest.get(i).toString(), of.getCoefficients().length);
			}
			
			SimplexWetzel simplex = new SimplexWetzel(r, of);
			SimplexWetzelInteger si = new SimplexWetzelInteger(new SimplexWetzel(r, of));
	 
			// return HTTP response 200 in case of success
			if (flagInt) {
				return Response.status(200).header("Access-Control-Allow-Origin", "*")
			            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
			            .header("Access-Control-Allow-Credentials", "true")
			            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			            .header("Access-Control-Max-Age", "1209600").
			            entity(si.solveToJson().toString()).build();
			} else {
				return Response.status(200).header("Access-Control-Allow-Origin", "*")
			            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
			            .header("Access-Control-Allow-Credentials", "true")
			            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			            .header("Access-Control-Max-Age", "1209600").
			            entity(simplex.solveToJson().toString()).build();
			}

			
			
		}  catch (Exception e) {
			
				
				jo = new JSONObject("{ \"code\":"+400+", \"error\":\""+e.getLocalizedMessage()+"\"}");
				return Response.status(400)
						 .header("Access-Control-Allow-Origin", "*")
				            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				            .header("Access-Control-Allow-Credentials", "true")
				            .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				            .header("Access-Control-Max-Age", "1209600")
				            .entity(jo.toString()).build();

		}
		


	  }
	 
	 
	  
	  
}